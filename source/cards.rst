Decks
=====

Il y a plusieurs manières de jouer.

- Un deck communs
- Un deck par équipe
- Un deck par personnage

.. note::

    Le type de deck est soit imposé par la league ou doit être choisis par les catcheurs avant le match.

Types de cartes
---------------

Matériel
~~~~~~~~

Des cartes de matériel ajoutent un peu  de piment. On y retrouve la classique chaise pliable, mais aussi des tables et des échelles.

Certaines cartes permettent plusieurs modes d'utilisation, mais un seul est utilisable (la carte doit être mis dans la défosse après utilisation).

- Chaise pliante
- Echelle
- Table
- Batte de baseball

**Note** : Les cartes de matériel peuvent ne pas être autorisées suivant les leagues.

Techniques spéciales
~~~~~~~~~~~~~~~~~~~~

Les techniques spéciales infligent plus ou moins de dégâts et son plus ou moins difficiles à sortir.

Elles nécessitent des jets sur un ou plusieurs attributs.

Certaines cartes doivent être faites par deux joueurs. Dans ce cas, la technique est réalisée lors du tour du dernier joueur et le premier « passe » virtuellement sont tour.
Ce type de carte permet d'infliger beaucoup plus de dégats, mais nécessite que chaque joueur réussisse les jets d'attributs, au risque de perdre un tour chacun.

Les techniques sont plus ou moins conseillées aux catcheurs suivants si elles sont aériennes, de force brut, de souplesse, … C'est là que réside toute la subtilité de la construction de son deck ou de celui de son équipe.

- Corde à linge
- Clef de bras
- Clef de jambe
- …

Soumissions
~~~~~~~~~~~

Les techniques de soumission sont là pour forcer le catcheur adverse à abandonner. Il s'agit de le bloquer épaules contre sol pendant 3×1D6.

Certaines sont plus faciles à executer que d'autres, mais elles sont toujours confronté aux points de KO de l'adversaire. Donc si il est en pleine forme, c'est quasiment impossible que ça fonctionne.

- Petit paquet
- …

Provocations
~~~~~~~~~~~~

Les provocations sonts l'âme du catch. Provoquez vos adversaires permet de récupérer des points de KO en plus d'être fun.

- Provocation (carte sans autre description, c'est au joueur d'inventer une provocation et de la balancer à ses adversaires)

Interruptions
~~~~~~~~~~~~~

Les interruptions peuvent être jouées en plus et permettent de contrer les effets d'une technique ou arrêter une soumission ou un KO.

- J'ai rien sentis (annule la technique subit)
- Accroche toi à la corde mon gars (annule une soumission)
- Serre moi fort (bonus sur une sortie de soumission)

Déroulement du jeu
~~~~~~~~~~~~~~~~~~

- Pioche
- Défausse
- Change le sens de jeu
- Joue deux fois
