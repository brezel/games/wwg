Règles
======

Ordre de jeu
------------

Pour savoir qui joue en premier, on lance 1D6. Le plus gros score commence et on tourne dans le sens des aiguilles d'une montre.

Catcheurs
---------

Chaque joueur incarne un catcheur. Celui-ci est représenté par un nom de scéne et des attributs. Ce sont ces attributs qui sont utilisés pour les jets de dés.

- Force (F)
- Endurance (E)
- Vitesse (V)
- Agilité (A)

Chaque catcheur posséde également un nombre de points de KO. Il correspond à la quantité de « dégats » qu'il peut encaisser jusqu'à être KO.

Pour les prises de soumissions, plus les points de KO sont élevés, plus il sera difficile de sortir de la prise.

Fiche de catcheur
~~~~~~~~~~~~~~~~~

Le jeu viens avec quelques catcheurs prétirés, mais les joueurs sont encouragés à créer leurs propres personnages pour plus de fun.

Pour créeur un catcheur, il suffit d'attribuer 5D6 entre les différenets attributs. Un dès peut être cassé. Dans ce cas, il se tranrforme en 4 points.

Les points non attribués peuvent être ajoutés au points de KO qui sont à l'origine de 10.

Ainsi il est possible d'avoir :

- F: 1D6, E: 1D6, V: 1D6, A: 1D6, KO: 14
- F: 1D6+1, E: 1D6+3, V: 1D6, A: 1D6, KO: 10

Réalisation d'une technique
---------------------------

Pour lancer une technique, le joueur doit regarder les conditions requises. Il s'agit de jets de dés sur une ou plusieurs attributs.

Chaque jet d'attribut doit être réussi pour lancer la technique.

Il suffit ensuite d'ajouter les points de dégats de la technique à la force du catcheur (qui est logiquement un lancé) et faire la différence avec le jet de défense du catcheur attaqué.

Cette différence est soustrait aux points de KO de l'adversaire.

KO
---

Si un catcheur voit ses points de KO passer à 0 il doit essayer de sortir du KO pour continuer. Si il échoue, il quitte la partie.

Sortir d'un KO
~~~~~~~~~~~~~~

Le joueur à 10 tentatives pour sortir un 6 avec 1D6 à chaque fois.

Si il fait un 6, il récupére 1 point de KO et reprend le combat.

Soumissions
-----------

Les cartes de soumissions sont légérement différentes car elles définissent une difficulté de libération.

C'est contre cette valeur que le catcheur ciblé doit faire des jets d'endurance + KO.

Il a 3 éssais pour se libérer.

Il se prend également autant de points de KO que de jets qu'il à dû faire pour s'en sortir.

- Se libére au premier jet : 1 point
- Se libére au second jet : 2 points
- Se libére au troisième jet : 3 points

Si le joueur ne se libére pas, il est déclaré vaincu.

