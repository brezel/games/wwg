Leagues
=======

Les leagues fournissent des règles de jeu en imposant certaines conditions et en limitant certaines pratiques.

Les joueurs sont invités à créer leurs propres leagues. Il n'y a aucune limite à la création

Brothers league
---------------

- Combat par équipe (2 Vs 2)
- Deck d'équipes commun

IKEA League
-----------

- Les cartes de techniques spéciales sont interdites
- Les cartes de matériel sont obligatoires (la création de carte spéciales avec des nom faisant suédois sont encouragées)
