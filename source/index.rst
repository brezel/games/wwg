Weird Wrestling Game
====================

.. toctree::
   :maxdepth: 2

   concept
   rules
   cards
   leagues
   belts
   resources
