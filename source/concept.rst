Concepte
========

Un jeu de carte / dés (avec une base OpenD6) où les joueurs incarnent des lutteurs.
Une pioche commune comporte des techniques spéciales qui peuvent être jouées à chaque tour.

Chaque tour, chaque joueur pioche une technique et en réalise une (si il n'utilise pas une technique, il peut se défausser et repiocher).

Les joueurs se font éliminer si ils sont soumis pendant 3D6 ou KO pendant 10D6.